// JS
$(document).ready(function(){
    // Menu button
	$('#navIcon').click(function(){
		$(this).addClass('open');
        $('.fullMenu').toggleClass('openMenu');
        $('header').toggleClass('menuWrap');
        $('.midnightHeader.grey').addClass('noBg');
	});
    $('#navIconClose').click(function(){
        $(this).removeClass('open');
		$('#navIcon').removeClass('open');
        $('.fullMenu').removeClass('openMenu');
        $('header').removeClass('menuWrap');
        $('.midnightHeader.grey').removeClass('noBg');
	});
    // About link remove menu
	$('#aboutLink').click(function(){
		$('#navIcon').removeClass('open');
        $('.fullMenu').toggleClass('openMenu');
        $('header').removeClass('menuWrap');
        $('.midnightHeader').removeClass('noBg');
	});
    // Smooth page scroll to ID
    $("a[href*='#']").mPageScroll2id();
    // Site wide accordion
    $('.accordion').accordion({
        "transitionSpeed": 400
    });
    // Variable header colors
    $('header').midnight();
    // Hero image slider
    $(".royalSlider").royalSlider({
    	autoScaleSlider: true,
    	autoHeight: false,
    	arrowsNav: true,
        imageAlignCenter: false,
        globalCaption: true,
        controlNavigation: 'none',
        imageScaleMode: 'none',
        imageAlignCenter: false,
        keyboardNavEnabled: true,
        slidesSpacing: 0,
        usePreloader: false,
        transitionSpeed: 600,
        transitionType: 'fade',
        sliderDrag: true,
        loop: true,
        loopRewind: true,
        autoPlay: {
    		enabled: true,
            delay: 6000,
    		pauseOnHover: false
    	}
    });
    $(".portSlider").royalSlider({
    	autoScaleSlider: true,
    	autoHeight: true,
    	arrowsNav: true,
        globalCaption: false,
        controlNavigation: 'none',
        imageScaleMode: 'none',
        imageAlignCenter: false,
        keyboardNavEnabled: true,
        slidesSpacing: 0,
        usePreloader: false,
        transitionSpeed: 600,
        transitionType: 'fade',
        sliderDrag: true,
        loop: true,
        loopRewind: true,
        autoPlay: {
    		enabled: false,
    		pauseOnHover: false
    	}
    });
});
