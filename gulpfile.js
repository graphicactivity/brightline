// Require

var gulp    = require('gulp'),
    gutil   = require('gulp-util'),
    sass    = require('gulp-sass'),
    concat  = require('gulp-concat'),
    prefix  = require('gulp-autoprefixer'),
    postcss = require('gulp-postcss'),
    rename  = require('gulp-rename'),
    uglify  = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps');


gulp.task('sass', function(){
  return gulp.src('scss/**/*.scss')
  .pipe(sourcemaps.init())
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(sourcemaps.write())
  .pipe(prefix({
            browsers: ['last 2 versions'],
            cascade: false
        }))
  .pipe(gulp.dest('web/assets/styles'));
});

gulp.task('scripts', function() {
  return gulp.src('js/*.js')
  .pipe(concat('main.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('web/assets/js'));
});

gulp.task('watch', function () {
  gulp.watch('scss/**/*.scss', ['sass']);
  gulp.watch('js/*.js', ['scripts']);
});
